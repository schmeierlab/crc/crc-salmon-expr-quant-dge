cat c2.all.v6.1.entrez.gmt | egrep REACTOME > crc.gmt
cat c2.all.v6.1.entrez.gmt | egrep KEGG >> crc.gmt
cat c2.all.v6.1.entrez.gmt | egrep '^PID' >> crc.gmt
cat c2.all.v6.1.entrez.gmt | egrep BIOCARTA >> crc.gmt
# add hallmark genes
cat h.all.v6.1.entrez.gmt >> crc.gmt
# add GO bio process
cat c5.bp.v6.1.entrez.gmt >> crc.gmt

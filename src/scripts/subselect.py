#!/usr/bin/env python
"""
NAME: subselect
=========

DESCRIPTION
===========

INSTALLATION
============

USAGE
=====

VERSION HISTORY
===============

0.0.1    2018    Initial version.

LICENCE
=======
2018, copyright Sebastian Schmeier, (s.schmeier@gmail.com), https://sschmeier.com

template version: 1.9 (2017/12/08)
"""
from signal import signal, SIGPIPE, SIG_DFL
import sys
import os
import os.path
import argparse
import csv
import collections
import gzip
import bz2
import zipfile
import time

# When piping stdout into head python raises an exception
# Ignore SIG_PIPE and don't throw exceptions on it...
# (http://docs.python.org/library/signal.html)
signal(SIGPIPE, SIG_DFL)

__version__ = '0.0.1'
__date__ = '2018'
__email__ = 's.schmeier@gmail.com'
__author__ = 'Sebastian Schmeier'

# For color handling on the shell
try:
    from colorama import init, Fore, Style
    # INIT color
    # Initialise colours for multi-platform support.
    init()
    reset=Fore.RESET
    colors = {'success': Fore.GREEN, 'error': Fore.RED, 'warning': Fore.YELLOW, 'info':''}
except ImportError:
    sys.stderr.write('colorama lib desirable. Install with "conda install colorama".\n\n')
    reset=''
    colors = {'success': '', 'error': '', 'warning': '', 'info':''}

def alert(atype, text, log):
    textout = '%s [%s] %s\n' % (time.strftime('%Y%m%d-%H:%M:%S'),
                                atype.rjust(7),
                                text)
    log.write('%s%s%s' % (colors[atype], textout, reset))
    if atype=='error': sys.exit()
        
def success(text, log=sys.stderr):
    alert('success', text, log)
    
def error(text, log=sys.stderr):
    alert('error', text, log)
    
def warning(text, log=sys.stderr):
    alert('warning', text, log)
    
def info(text, log=sys.stderr):
    alert('info', text, log)  

    
def parse_cmdline():
    """ Parse command-line args. """
    ## parse cmd-line -----------------------------------------------------------
    description = 'Read delimited file.'
    version = 'version %s, date %s' % (__version__, __date__)
    epilog = 'Copyright %s (%s)' % (__author__, __email__)

    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument('--version',
                        action='version',
                        version='%s' % (version))

    parser.add_argument(
        'files',
        metavar='FILE',
        nargs="+",
        help=
        'Delimited file.')
    parser.add_argument('-d',
                        '--delimiter',
                        metavar='STRING',
                        dest='delimiter_str',
                        default='\t',
                        help='Delimiter used in file.  [default: "tab"]')
    parser.add_argument('--fdr',
                        type=float,
                        metavar='FLOAT',
                        dest='fdr',
                        default=0.05,
                        help='FDR threshold')
    parser.add_argument('--fc',
                        type=float,
                        metavar='FLOAT',
                        dest='fc',
                        default=2,
                        help='Log2 Fold-change threshold')
    

    # if no arguments supplied print help
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
    
    args = parser.parse_args()
    return args, parser


def load_file(filename):
    """ LOADING FILES """
    if filename in ['-', 'stdin']:
        filehandle = sys.stdin
    elif filename.split('.')[-1] == 'gz':
        filehandle = gzip.open(filename, 'rt')
    elif filename.split('.')[-1] == 'bz2':
        filehandle = bz2.BZFile(filename)
    elif filename.split('.')[-1] == 'zip':
        filehandle = zipfile.Zipfile(filename)
    else:
        filehandle = open(filename)
    return filehandle


def main():
    """ The main funtion. """
    args, parser = parse_cmdline()
    outfilelist = []
    for file in args.files:
        try:
            fileobj = load_file(file)
        except:
            error('Could not load map file. EXIT.')

        
        f = file.replace('.txt.gz', '.subselected.txt.gz')
        outfileobj = gzip.open(f, 'wt')
        outfilelist.append(f)
        
        # delimited file handler
        csv_reader_obj = csv.reader(fileobj, delimiter=args.delimiter_str)
        header = next(csv_reader_obj)
        outfileobj.write("%s\n"%"\t".join(header))

        res = []
        for a in csv_reader_obj:
            if float(a[2]) >= args.fc and float(a[5]) < args.fdr:
                res.append(a)

        # sort after fc
        res2 = [[float(x[2])] + x for x in res]
        res2.sort()
        res2.reverse()
        res = [x[1:] for x in res2]
            
        for a in res:    
            outfileobj.write("%s\n" % ("\t".join(a)))
        fileobj.close()
        outfileobj.close()

    # print out produced files
    sys.stdout.write('%s\n' % ('\n'.join(outfilelist)))        
    return


if __name__ == '__main__':
    sys.exit(main())


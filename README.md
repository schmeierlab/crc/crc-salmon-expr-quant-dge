# PROJECT: crc-salmon-expr-quant-dge
AUTHOR: Sebastian Schmeier (s.schmeier@gmail.com) 
DATE: 2018-03-18

## OVERVIEW

We use Salmon to quantify expression and edgeR to preform a differential gene expression analysis.
We use clusterProfiler to perfm term enrichment analysis for biological categories.

## INSTALL

```bash
# Install miniconda
# LINUX:
curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
# MACOSX:
curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
bash Miniconda3-latest-MacOSX-x86_64.sh

# Add conda dir to PATH
echo 'export PATH="~/miniconda3/bin:$PATH"' >> ~/.bashrc
echo 'export PATH="~/miniconda3/bin:$PATH"' >> ~/.zshrc

# Make env
# this environment contains the bare base of what is required to run snakemake
conda env create --name snakemake --file envs/snakemake.yaml
source activate snakemake
```

## RUN SNAKEMAKE-WORKFLOW

```bash
# Run the snakemake workflow, log time and commands 
snakemake -p --use-conda --use-singularity --singularity-args "--bind /mnt/disk2" --jobs 10 > logs/run.stdout 2> logs/run.stderr
```



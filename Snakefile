import glob, os, os.path, re, csv
from os.path import join


## define global Singularity image for reproducibility
## needs --use-singularity to run all jobs in container
singularity: "docker://continuumio/miniconda3:4.5.4"

## Steps:
## 1. tximport  
## 2. deg
## 3. enrichment analyses

## LOAD VARIABLES FROM CONFIGFILE ----------
configfile: "config.yml"

BASEDIR = config["basedir"]
LOGDIR = config["logdir"]
BENCHMARKDIR = config["benchmarkdir"]
WRAPPERDIR = config["wrapperdir"]
SCRIPTDIR = config["scriptdir"]
ENVS = config["envs"]

## INPUTS
SAMPLESHEET = config["samplesheet"]
EXCLUDE = config["exclude"]
QUANTSDIR = config["quants"]
MAP = config["txmap"]

## OUTPUTS
### DGE
DGEDIR = config["dgedir"]
DGEPAIRWISE = join(DGEDIR,"pairwise")
FC = config["fc"]
FDR = config["fdr"]
TOP = config["top"]
### ENRICH
GSEAFDR = config["gseafdr"]
GSEAGMT = config["gmtfile"]
##------------------------------------------

## Pseudo-rule to list the final targets,
## so that the whole workflow is run.
rule all:
    input:
        [join(DGEDIR, "pairwise_enrichement_misgdb.done"),
         join(DGEDIR, "pairwise_enrichement_misgdb_combined_up.done"),
         join(DGEDIR, "pairwise_enrichement_misgdb_combined_dn.done")]

        
rule make_final_samplesheet:
    input:
        SAMPLESHEET,
        EXCLUDE
    output:
        join(DGEDIR, "sample_list.txt")
    log:
        join(LOGDIR, "make_final_samplesheet.log")
    run:
        reader = csv.reader(open(input[1], "r"), delimiter = '\t')
        exclude = {}
        for a in reader: exclude[a[0]] = None
        # open outfile
        out = open(output[0], 'w')
        reader = csv.reader(open(input[0], "r"), delimiter = '\t')
        for a in reader:
            sample = a[0]
            # exclude if in exclude list
            if sample in exclude: continue
            # exclude if not classified
            if a[1] != 'NA':
                filename = os.path.abspath(join(join(QUANTSDIR, sample),"quant.sf"))
                assert os.path.isfile(filename) == True
                a.append(filename)
                out.write('%s\n' % '\t'.join(a))
        out.close()

        
rule edger_dge:
    input:
        samplelist=join(DGEDIR, "sample_list.txt"),
        map=MAP
    output:
        touch(join(DGEDIR, "dge.done"))
    log:
        join(LOGDIR, "edger_dge.log")
    benchmark:
        join(BENCHMARKDIR, 'edger_dge.txt')
    params:
        script=join(SCRIPTDIR, "salmon_edgeR_genes_DGE.R"),
        outdir=DGEDIR,
        log="ihs"
    conda:
        join(ENVS, "tximport-20180411.yaml")
    shell:
        "Rscript --vanilla --slave {params.script} {input.map} {input.samplelist} {params.outdir} {params.log} 2> {log}; "


rule attach_info:
    input:
        join(DGEDIR, "dge.done")
    output:
        touch(join(DGEDIR, "attach_info.done"))
    params:
        files = join(DGEDIR, 'edgeR*.txt'),
        map = MAP,
        script= join(SCRIPTDIR, "attach_info.py"),
    shell:
        "python {params.script} --map {params.map} {params.files}"

        
## rule subselect:
##     # subselect based on fdr and fc
##     input:
##         join(DGEDIR, "attach_info.done")
##     output:
##         join(DGEDIR, "subselected.files.txt")
##     params:
##         files = join(DGEDIR, 'edgeR*.info.txt.gz'),
##         script= join(SCRIPTDIR, "subselect.py"),
##         fdr = FDR,
##         fc = FC
##     shell:
##         "python {params.script} --fdr {params.fdr} --fc {params.fc} {params.files} > {output};"

        
## rule combine:
##     # we find per subselcted file the values of the other CMS types
##     input:
##         join(DGEDIR, "subselected.files.txt")
##     output:
##         join(DGEDIR, "genes-cms-vs-other-combined.txt.gz")
##     params:
##         script= join(SCRIPTDIR, "combine.py"),
##         path=DGEDIR
##     shell:
##         "python {params.script} {params.path} | gzip > {output}"

        
## rule enrichement_misgdb:
##     input:
##         join(DGEDIR, "subselected.files.txt")
##     output:
##         touch(join(DGEDIR, "enrichment_msigdb.done"))
##     log:
##         join(LOGDIR, "enrichment_msigdb.log")
##     benchmark:
##         join(BENCHMARKDIR, 'enrichment_msigdb.txt')
##     conda:
##         join(ENVS, "clusterprofiler-20180411.yaml")
##     params:
##         script = join(SCRIPTDIR, "enrichment_msigdb.R"),
##         gmt=GSEAGMT,
##         fdr=GSEAFDR
##     shell:
##         """
##         for f in `cat {input}`; do
##             zcat ${{f}} | cut -f 2 | tail -n +2 | Rscript --vanilla --slave {params.script} {params.fdr} {params.gmt} ${{f%.txt.gz}}.enrich-msigdb.txt ${{f%.txt.gz}}.enrich-msigdb.pdf; 
##         done 2> {log}
##         """


## rule enrichement_misgdb_combined:
##     input:
##         join(DGEDIR, "subselected.files.txt"),
##         join(DGEDIR, "enrichment_msigdb.done")
##     output:
##         touch(join(DGEDIR, "enrichment_msigdb_combined.done")),
##         out = join(DGEDIR, "cms1-cms2-cms3-cms4.enrich-msigdb.txt"),
##         outplot = join(DGEDIR, "cms1-cms2-cms3-cms4.enrich-msigdb.pdf"),
##     log:
##         join(LOGDIR, "enrichment_msigdb_combined.log")
##     benchmark:
##         join(BENCHMARKDIR, 'enrichment_msigdb_combined.txt')
##     conda:
##         join(ENVS, "clusterprofiler-20180411.yaml")
##     params:
##         script = join(SCRIPTDIR, "enrichment_msigdb_compare.R"),
##         gmt=GSEAGMT,
##         fdr=GSEAFDR,
##         files=[join(DGEDIR,"edgeR_CMS1-VS-OTHER.info.subselected.txt.gz"),
##                join(DGEDIR,"edgeR_CMS2-VS-OTHER.info.subselected.txt.gz"),
##                join(DGEDIR,"edgeR_CMS3-VS-OTHER.info.subselected.txt.gz"),
##                join(DGEDIR,"edgeR_CMS4-VS-OTHER.info.subselected.txt.gz")]
##     shell:
##         """
##         Rscript --vanilla --slave {params.script} {params.fdr} {params.gmt} {params.files} {output.out} {output.outplot} 2> {log}
##         """


# PAIRWISE
rule pairwise:
    # we find per subselcted file the values of the other CMS types
    input:
        join(DGEDIR, "attach_info.done")
    output:
        touch(join(DGEDIR, "pairwise.done"))
    params:
        script = join(SCRIPTDIR, "pairwise.py"),
        path = DGEDIR
    shell:
        "python {params.script} {params.path} {params.path}"

rule pairwise_select:
    input:
        join(DGEDIR, "pairwise.done")
    output:
        touch(join(DGEDIR, "pairwise_select.done"))
    params:
        script = join(SCRIPTDIR, "subselect_pairwise.py"),
        files = join(DGEDIR, "CMS[1234].txt")
    shell:
        """
        python {params.script} --top {TOP} --fc {FC} --fdr {FDR} {params.files}
        """

rule pairwise_enrichement_misgdb:
    input:
        join(DGEDIR, "pairwise_select.done")
    output:
        touch(join(DGEDIR, "pairwise_enrichement_misgdb.done"))
    log:
        join(LOGDIR, "pairwise_enrichement_misgdb.log")
    benchmark:
        join(BENCHMARKDIR, 'pairwise_enrichement_misgdb.txt')
    conda:
        join(ENVS, "clusterprofiler-20180411.yaml")
    params:
        script = join(SCRIPTDIR, "enrichment_msigdb.R"),
        gmt=GSEAGMT,
        fdr=GSEAFDR,
        files = join(DGEDIR, "CMS*_*.txt")
    shell:
        """
        for f in `ls {params.files}`; do
            cat ${{f}} | cut -f 2 | tail -n +2 | Rscript --vanilla --slave {params.script} {params.fdr} {params.gmt} ${{f%.txt.gz}}.enrich-msigdb.txt ${{f%.txt.gz}}.enrich-msigdb.pdf; 
        done 2> {log}
        """

rule pairwise_enrichement_misgdb_combined_up:
    input:
        join(DGEDIR, "pairwise_enrichement_misgdb.done")
    output:
        touch(join(DGEDIR, "pairwise_enrichement_misgdb_combined_up.done")),
        out = join(DGEDIR, "cms1-cms2-cms3-cms4.enrich-msigdb_up.txt"),
        outplot = join(DGEDIR, "cms1-cms2-cms3-cms4.enrich-msigdb_up.pdf"),
    log:
        join(LOGDIR, "pairwise_enrichement_misgdb_combined_up.log")
    benchmark:
        join(BENCHMARKDIR, 'pairwise_enrichement_misgdb_combined_up.txt')
    conda:
        join(ENVS, "clusterprofiler-20180411.yaml")
    params:
        script = join(SCRIPTDIR, "enrichment_msigdb_compare.R"),
        gmt=GSEAGMT,
        fdr=GSEAFDR,
        files=join(DGEDIR, "CMS*_up.txt")
    shell:
        """
        Rscript --vanilla --slave {params.script} {params.fdr} {params.gmt} {params.files} {output.out} {output.outplot} 2> {log}
        """

rule pairwise_enrichement_misgdb_combined_dn:
    input:
        join(DGEDIR, "pairwise_enrichement_misgdb.done")
    output:
        touch(join(DGEDIR, "pairwise_enrichement_misgdb_combined_dn.done")),
        out = join(DGEDIR, "cms1-cms2-cms3-cms4.enrich-msigdb_dn.txt"),
        outplot = join(DGEDIR, "cms1-cms2-cms3-cms4.enrich-msigdb_dn.pdf"),
    log:
        join(LOGDIR, "pairwise_enrichement_misgdb_combined_dn.log")
    benchmark:
        join(BENCHMARKDIR, 'pairwise_enrichement_misgdb_combined_dn.txt')
    conda:
        join(ENVS, "clusterprofiler-20180411.yaml")
    params:
        script = join(SCRIPTDIR, "enrichment_msigdb_compare.R"),
        gmt=GSEAGMT,
        fdr=GSEAFDR,
        files=join(DGEDIR, "CMS*_dn.txt")
    shell:
        """
        Rscript --vanilla --slave {params.script} {params.fdr} {params.gmt} {params.files} {output.out} {output.outplot} 2> {log}
        """
        
rule clean:
    shell:
        "rm -rf {BASEDIR}/*"
        
        

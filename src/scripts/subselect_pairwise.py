#!/usr/bin/env python
"""
NAME: subselect
=========

DESCRIPTION
===========

INSTALLATION
============

USAGE
=====

VERSION HISTORY
===============

0.0.1    2018    Initial version.

LICENCE
=======
2018, copyright Sebastian Schmeier, (s.schmeier@gmail.com), https://sschmeier.com

template version: 1.9 (2017/12/08)
"""
from signal import signal, SIGPIPE, SIG_DFL
import sys
import os
import os.path
import argparse
import csv
import collections
import gzip
import bz2
import zipfile
import time

# When piping stdout into head python raises an exception
# Ignore SIG_PIPE and don't throw exceptions on it...
# (http://docs.python.org/library/signal.html)
signal(SIGPIPE, SIG_DFL)

__version__ = '0.0.1'
__date__ = '2018'
__email__ = 's.schmeier@gmail.com'
__author__ = 'Sebastian Schmeier'

# For color handling on the shell
try:
    from colorama import init, Fore, Style
    # INIT color
    # Initialise colours for multi-platform support.
    init()
    reset=Fore.RESET
    colors = {'success': Fore.GREEN, 'error': Fore.RED, 'warning': Fore.YELLOW, 'info':''}
except ImportError:
    sys.stderr.write('colorama lib desirable. Install with "conda install colorama".\n\n')
    reset=''
    colors = {'success': '', 'error': '', 'warning': '', 'info':''}

def alert(atype, text, log):
    textout = '%s [%s] %s\n' % (time.strftime('%Y%m%d-%H:%M:%S'),
                                atype.rjust(7),
                                text)
    log.write('%s%s%s' % (colors[atype], textout, reset))
    if atype=='error': sys.exit()
        
def success(text, log=sys.stderr):
    alert('success', text, log)
    
def error(text, log=sys.stderr):
    alert('error', text, log)
    
def warning(text, log=sys.stderr):
    alert('warning', text, log)
    
def info(text, log=sys.stderr):
    alert('info', text, log)  

    
def parse_cmdline():
    """ Parse command-line args. """
    ## parse cmd-line -----------------------------------------------------------
    description = 'Read delimited file.'
    version = 'version %s, date %s' % (__version__, __date__)
    epilog = 'Copyright %s (%s)' % (__author__, __email__)

    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument('--version',
                        action='version',
                        version='%s' % (version))

    parser.add_argument(
        'files',
        metavar='FILE',
        nargs="+",
        help=
        'Delimited file.')
    parser.add_argument('-d',
                        '--delimiter',
                        metavar='STRING',
                        dest='delimiter_str',
                        default='\t',
                        help='Delimiter used in file.  [default: "tab"]')
    parser.add_argument('--fdr',
                        type=float,
                        metavar='FLOAT',
                        dest='fdr',
                        default=0.05,
                        help='FDR threshold')
    parser.add_argument('--fc',
                        type=float,
                        metavar='FLOAT',
                        dest='fc',
                        default=2,
                        help='Log2 Fold-change threshold')
    parser.add_argument('--top',
                        type=int,
                        metavar='INT',
                        dest='top',
                        default=None,
                        help='Only consider top number of genes with highest avg fc. [default: all genes]')
    

    # if no arguments supplied print help
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
    
    args = parser.parse_args()
    return args, parser


def load_file(filename):
    """ LOADING FILES """
    if filename in ['-', 'stdin']:
        filehandle = sys.stdin
    elif filename.split('.')[-1] == 'gz':
        filehandle = gzip.open(filename, 'rt')
    elif filename.split('.')[-1] == 'bz2':
        filehandle = bz2.BZFile(filename)
    elif filename.split('.')[-1] == 'zip':
        filehandle = zipfile.Zipfile(filename)
    else:
        filehandle = open(filename)
    return filehandle


def main():
    """ The main funtion. """
    args, parser = parse_cmdline()

    fdrcols = [3,5,7]
    fccols = [2,4,6]
    for file in args.files:
        try:
            fileobj = load_file(file)
        except:
            error('Could not load file {}. EXIT.'.format(fileee))

        
        fup = open(file.replace('.txt', '_up.txt'), "w")
        fdn = open(file.replace('.txt', '_dn.txt'), "w")

        
        # delimited file handler
        csv_reader_obj = csv.reader(fileobj, delimiter=args.delimiter_str)
        header = next(csv_reader_obj)
        fup.write("{}\n".format("\t".join(header)))
        fdn.write("{}\n".format("\t".join(header)))

        resUP = []
        resDN = []
        for a in csv_reader_obj:
            iFDR = sum([1 if float(a[i]) < args.fdr else 0 for i in fdrcols])
            if iFDR < 3: continue
            iFCup  = sum([1 if float(a[i]) >= args.fc else 0 for i in fccols])
            iFCdn  = sum([1 if float(a[i]) <= -args.fc else 0 for i in fccols])

            avgFC = sum([float(a[i]) for i in fccols])/3.
            if iFCup == 3:
                resUP.append([avgFC] + a)
            elif iFCdn == 3:
                resDN.append([avgFC] + a)

        resUP.sort()
        resUP.reverse()
        if args.top > 0:
            for a in resUP[0:args.top]:
                fup.write("{}\n".format("\t".join(a[1:])))
        else:
            for a in resUP:
                fup.write("{}\n".format("\t".join(a[1:])))
        fup.close()

        resDN.sort()
        if args.top > 0:
            for a in resDN[0:args.top]:
                fdn.write("{}\n".format("\t".join(a[1:])))
        else:
            for a in resDN:
                fdn.write("{}\n".format("\t".join(a[1:])))
        fdn.close()

        fileobj.close()
                
    return


if __name__ == '__main__':
    sys.exit(main())


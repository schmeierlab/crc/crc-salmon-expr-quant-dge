#!/usr/bin/env python
"""
NAME: subselect
=========

DESCRIPTION
===========

INSTALLATION
============

USAGE
=====

VERSION HISTORY
===============

0.0.1    2018    Initial version.

LICENCE
=======
2018, copyright Sebastian Schmeier, (s.schmeier@gmail.com), https://sschmeier.com

template version: 1.9 (2017/12/08)
"""
from signal import signal, SIGPIPE, SIG_DFL
import sys
import os
import os.path
import argparse
import csv
import collections
import gzip
import bz2
import zipfile
import time

# When piping stdout into head python raises an exception
# Ignore SIG_PIPE and don't throw exceptions on it...
# (http://docs.python.org/library/signal.html)
signal(SIGPIPE, SIG_DFL)

__version__ = '0.0.1'
__date__ = '2018'
__email__ = 's.schmeier@gmail.com'
__author__ = 'Sebastian Schmeier'

# For color handling on the shell
try:
    from colorama import init, Fore, Style
    # INIT color
    # Initialise colours for multi-platform support.
    init()
    reset=Fore.RESET
    colors = {'success': Fore.GREEN, 'error': Fore.RED, 'warning': Fore.YELLOW, 'info':''}
except ImportError:
    sys.stderr.write('colorama lib desirable. Install with "conda install colorama".\n\n')
    reset=''
    colors = {'success': '', 'error': '', 'warning': '', 'info':''}

def alert(atype, text, log):
    textout = '%s [%s] %s\n' % (time.strftime('%Y%m%d-%H:%M:%S'),
                                atype.rjust(7),
                                text)
    log.write('%s%s%s' % (colors[atype], textout, reset))
    if atype=='error': sys.exit()
        
def success(text, log=sys.stderr):
    alert('success', text, log)
    
def error(text, log=sys.stderr):
    alert('error', text, log)
    
def warning(text, log=sys.stderr):
    alert('warning', text, log)
    
def info(text, log=sys.stderr):
    alert('info', text, log)  

    
def parse_cmdline():
    """ Parse command-line args. """
    ## parse cmd-line -----------------------------------------------------------
    description = 'Read delimited file.'
    version = 'version %s, date %s' % (__version__, __date__)
    epilog = 'Copyright %s (%s)' % (__author__, __email__)

    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument('--version',
                        action='version',
                        version='%s' % (version))

    parser.add_argument('path',
                         metavar='PATH',
                         help='Path to result-files.')
    parser.add_argument('outdir',
                         metavar='PATH',
                         help='Outdir.')
    
    # if no arguments supplied print help
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
    
    args = parser.parse_args()
    return args, parser


def load_file(filename):
    """ LOADING FILES """
    if filename in ['-', 'stdin']:
        filehandle = sys.stdin
    elif filename.split('.')[-1] == 'gz':
        filehandle = gzip.open(filename, 'rt')
    elif filename.split('.')[-1] == 'bz2':
        filehandle = bz2.BZFile(filename)
    elif filename.split('.')[-1] == 'zip':
        filehandle = zipfile.Zipfile(filename)
    else:
        filehandle = open(filename)
    return filehandle


def main():
    """ The main funtion. """
    args, parser = parse_cmdline()

    cmsTypes = ['CMS1', 'CMS2', 'CMS3', 'CMS4']
    infoTemplate = os.path.join(args.path, 'edgeR_%s-VS-%s.info.txt.gz')

    if not os.path.exists(args.outdir):
        os.mkdir(args.outdir)
    
    for cmsA in cmsTypes:
        ref = {}
        genes = {}
        for cmsB in cmsTypes:
            if cmsA == cmsB:
                continue
        
            if (cmsA,cmsB) not in ref:
                ref[(cmsA,cmsB)] = {}
            
            try:
                fileobj = load_file(infoTemplate % (cmsA,cmsB))
            except:
                error('Could not load info file. EXIT.')

            csv_reader_obj = csv.reader(fileobj, delimiter='\t')
            header = next(csv_reader_obj)
            for a in csv_reader_obj:
                if (cmsA,cmsB) not in ref:
                    ref[(cmsA,cmsB)] = {}

                gene = (a[0],a[1])
                ref[(cmsA,cmsB)][gene] = [a[2], a[5]]  # fc , fdr
                genes[gene] = None
            
            fileobj.close()

        genes = list(genes.keys())
        genes.sort()
        keys = list(ref.keys())
        keys.sort()
        outfileobj = open(os.path.join(args.outdir, "{}.txt".format(cmsA)), 'w')
        # header
        out = ['Symbol','Gene']
        out = out + ['{}_{}_fc\t{}_{}_fdr'.format(k[0], k[1], k[0], k[1]) for k in keys]
        outfileobj.write('{}\n'.format('\t'.join(out)))
        
        for gene in genes:
            out = [gene[0], gene[1]]
            for k in keys:
                if gene in ref[k]:
                    out = out + ref[k][gene]
                else:
                    out = out + ['-', '-']
            outfileobj.write('%s\n' % '\t'.join(out))
    
        outfileobj.close()        

    return


if __name__ == '__main__':
    sys.exit(main())

